from abc import ABC, abstractmethod
from datetime import date

class Person(ABC):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._requests = []

    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self):
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self):
        pass


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "No requests to check"

    def addUser(self):
        return "Cannot add user, not authorized"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"


class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)
        self._members = []
    
    def get_members(self):
        return self._members

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "No requests to check"

    def addUser(self):
        return "Cannot add user, not authorized"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addMember(self, employee):
        self._members.append(employee)


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "No requests to check"

    def addUser(self):
        return "User has been added"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"


class Request:
    def __init__(self, name, requester, dateRequest):
        self._name = name
        self._requester = requester
        self._dateRequest = dateRequest
        self.set_status = "Open"

    def updateRequest(self, status):
        self.set_status = status

    def closeRequest(self):
        self.set_status = "Closed"
        return f"Request {self._requester} has been {self.get_status}"

    def cancelRequest(self):
        self.set_status = "Canceled"

#Test Cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing") 
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing") 
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales") 
employee4 = Employee("Brandon", "Smith", "brandon@mail.com", "Sales") 
admin1 = Admin("Monika", "Justin", "monika@mail.com", "Marketing") 
teamLead1 = TeamLead ("Michael", "Specter", "smichael@@mail.com", "Sales") 
req1=Request( teamLead1,"New hire orientation","27-July-2021") 
req2= Request(employee1, "Laptop repair","21-July-2021")
assert employee1.getFullName() == "John Doe", "Full name should be John Doe" 
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin" 
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specier"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.get_status = "Closed"
print(req2.closeRequest())